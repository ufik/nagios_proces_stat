#!/usr/bin/python

import os, time

  # find out the pid by username.
  # "-o pid h" omits the header and just prints the pid
pid = os.popen('ps -C memtester -o pid h | tail -1').read().strip()

  # 14th column is utime, 15th column is stime:
  # The time the process has been scheduled in user/kernel mode
  # The time value is in jiffies. One jiffie is appox 1/100 second
  # see man proc for more info
stat = os.popen('cat /proc/%s/stat' % pid).read().strip()
cpu_time1=int(stat.split()[13]) + int(stat.split()[14])
time1=time.time()

time.sleep(1)
stat = os.popen('cat /proc/%s/stat' % pid).read().strip()
cpu_time2=int(stat.split()[13]) + int(stat.split()[14])
time2=time.time()
mem = int(stat.split()[22])/1024


print str(float(cpu_time2 - cpu_time1) / (time2 - time1)) + "%"
print str(mem) + "Kb"
